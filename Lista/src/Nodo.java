
public class Nodo<T> {
	
	private T elemento;
	private Nodo<T> siguiente;
	
	public Nodo(T valor){
		this.elemento = valor;
		this.setSiguiente(null);
	}
	
	protected T getElemento() {
		return this.elemento;
	}

	protected Nodo<T> getSiguiente() {
		return siguiente;
	}

	protected void setSiguiente(Nodo<T> siguiente) {
		this.siguiente = siguiente;
	}

}
