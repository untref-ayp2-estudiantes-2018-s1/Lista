
public class IteradorLista<T> {

	Nodo<T> actual; // Current position
	// Construct the list iterator

	IteradorLista(Nodo<T> elNodo) {
		this.actual = elNodo;
	}

	// Test if the current position is a valid position in
	public boolean esValido() {
		return actual != null;
	}

	// Return the item stored in the current position.
	public T obtener() {
		// Esta linea es una forma alternativa de escribir un if corto
		// Si esValido es true, devuelve actual.getElemento()
		// Si no, devuelve null
		return esValido() ? actual.getElemento() : null;
	}

	// Advance the current position to the next node in the list.
	public void avanzar() {
		if (esValido()) {
			actual = actual.getSiguiente();
		}
	}


}
