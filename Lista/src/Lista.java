
public class Lista<T> {

	private Nodo<T> primero;

	public Lista() {
		this.primero = new Nodo<T>(null);
	}

	public void insertarAlPrincipio(T dato) {
		
		IteradorLista <T> itr = zeroth ();
		
		Nodo <T> nodoNuevo = new Nodo <T> (dato);
		
		nodoNuevo.setSiguiente(itr.actual.getSiguiente());
		itr.actual.setSiguiente(nodoNuevo);

	}

	public void borrarElemento(T dato) {

		IteradorLista <T> itr = anterior (dato);
		
		if (itr != null) {
			
			itr.actual.setSiguiente(itr.actual.getSiguiente().getSiguiente());
		}
		

	}
	
	public void insertarDespuesDe (T buscado, T nuevo) {
		
		IteradorLista <T> itr = find (buscado);
		
		if (itr != null) {
			
			Nodo <T> nodoAux = itr.actual.getSiguiente();
			Nodo <T> nuevoNodo = new Nodo <T> (nuevo); 
			itr.actual.setSiguiente(nuevoNodo);
			nuevoNodo.setSiguiente(nodoAux);
			
		}
	}

	public IteradorLista<T> find(T x) {
		Nodo<T> itr = primero.getSiguiente();
		while (itr != null && !itr.getElemento().equals(x)) {
			itr = itr.getSiguiente();
		}
		return new IteradorLista<T>(itr);
	}

	public IteradorLista<T> first() {

		return new IteradorLista<T>(primero.getSiguiente());
	}

	public IteradorLista<T> zeroth() {

		return new IteradorLista<T>(primero);

	}
	
	public IteradorLista <T> anterior (T dato){
		
		Nodo<T> itr = primero;
		while (itr.getSiguiente() != null && !itr.getSiguiente().getElemento().equals(dato)) {
			itr = itr.getSiguiente();
		}
		return new IteradorLista<T>(itr);
	}
	
	public void imprimirLista () {
		
		IteradorLista <T> itr = first();
		
		while (itr.esValido()) {
			
			System.out.print(itr.actual.getElemento() + " ");
			itr.avanzar();
		}
		System.out.println();
	}
	

}
