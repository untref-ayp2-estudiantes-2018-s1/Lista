
public class MainLista {
	
	public static void main (String [] args) {
		
		Lista <Integer> lista = new Lista <Integer>();
		
		
		lista.insertarAlPrincipio(1);
		lista.imprimirLista();
		lista.insertarDespuesDe(1, 2);
		lista.imprimirLista();
		lista.insertarDespuesDe(2, 3);
		lista.imprimirLista();
		lista.insertarAlPrincipio(0);
		lista.imprimirLista();
		lista.borrarElemento(1);
		lista.imprimirLista();
	}

}
